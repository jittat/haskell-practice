
inst :: Ord a => a -> [a] -> [a]
inst x [] = [x]
inst x (y:ys) = if x <= y
  then (x:(y:ys))
  else (y:(inst x ys))

instSort :: Ord a => [a] -> [a]
instSort [] = []
instSort (x:xs) = inst x (instSort xs)

selMin :: Ord a => [a] -> (a, [a])
selMin [a] = (a,[])
selMin (x:xs) = let (mn, rest) = selMin xs
                in if mn < x
                   then (mn, (x:rest))
                   else (x, (mn:rest))

selSort :: Ord a => [a] -> [a]
selSort [] = []
selSort xs = let (mn, rest) = selMin xs
             in (mn:(selSort rest))

merge :: Ord a => [a] -> [a] -> [a]
merge [] [] = []
merge (x:xs) [] = (x:xs)
merge [] (y:ys) = (y:ys)
merge (x:xs) (y:ys) = if x <= y
                      then (x:(merge xs (y:ys)))
                      else (y:(merge (x:xs) ys))

split [] = ([], [])
split [y] = ([y],[])
split (x:y:ys) = let (ys1, ys2) = split ys
                 in ((x:ys1), (y:ys2))

mergeSort :: Ord a => [a] -> [a]
mergeSort [] = []
mergeSort [a] = [a]
mergeSort xs = let
  (left, right) = split xs
  lsorted = mergeSort left
  rsorted = mergeSort right
  in merge lsorted rsorted
  


