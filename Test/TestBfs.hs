import Graph.Graph
import Graph.Bfs

main = do
  g <- readGraph
  putStrLn (show g)
  putStrLn (show (bfs 1 g))
