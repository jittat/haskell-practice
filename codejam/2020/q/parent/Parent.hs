import Data.List

extractTimes :: String -> (Int, Int)
extractTimes s = let (a:b:_) = words s
                 in (read a :: Int, read b :: Int)

extractTestCase :: Int -> [String] -> [(Int, [(Int, Int)])] -> [(Int, [(Int, Int)])]
extractTestCase 0 _ out = reverse out
extractTestCase t (ns:rest) out = let n = (read ns :: Int)
                                  in (extractTestCase
                                       (t-1)
                                       (drop n rest)
                                       ((n, (map extractTimes (take n rest))):out))

dosolve :: Int -> Int -> [((Int, Int), Int)] -> [(Int, Char)] -> String
dosolve _ _ [] out = [c | (k,c) <- (sort out)]
dosolve tc tj (((s,t),k):rest) out = if s >= tc
                                 then dosolve t tj rest ((k,'C'):out)
                                 else if s >= tj
                                      then dosolve tc t rest ((k,'J'):out)
                                      else "IMPOSSIBLE"

solve :: (Int, [(Int, Int)]) -> Int -> String
solve (n,s) tt = let output = dosolve (-1) (-1) (sort (zip s [1..n])) []
                 in ("Case #" ++ (show tt) ++ ": " ++ output)

main :: IO ()
main = do contents <- getContents
          let ll = lines contents
          let (ts:ss) = ll
          let t = (read ts :: Int)
          let testcases = extractTestCase t ss []
          let results = [(solve s tt) | (s,tt) <- (zip testcases [1..t])]
          putStr (unlines results)

          
