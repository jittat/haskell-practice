dosolve :: Integer -> String -> String -> String
dosolve 0 [] out = reverse out
dosolve n [] out = dosolve (n-1) [] (')':out)
dosolve n (x:xs) out = let xnum = (read [x] :: Integer)
                       in if n == xnum
                          then dosolve n xs (x:out)
                          else if n < xnum
                               then dosolve (n+1) (x:xs) ('(':out)
                               else dosolve (n-1) (x:xs) (')':out)

solve :: String -> Integer -> String
solve s t = let out = dosolve 0 s []
            in "Case #" ++ (show t) ++ ": " ++ out

main :: IO ()
main = enumFromTo (1::Integer) <$> readLn >>= mapM_ docase
  where docase tt = do
          s <- getLine
          let res = solve s tt
          putStr (res ++ "\n")
