module DS.Queue
  ( Queue
  , outQueue
  , inStack
  , emptyQueue
  , initQueue
  , isQueueEmpty
  , extractFront
  , getFront
  , insertRear
  ) where

data Queue a = Queue { outQueue :: [a]
                     , inStack :: [a]
                     } deriving (Show)

emptyQueue = (Queue [] [])

initQueue lst = Queue lst []

isQueueEmpty (Queue [] []) = True
isQueueEmpty (Queue _ _) = False

extractFront (Queue (x:xs) ys) = (x, Queue xs ys)
extractFront (Queue [] ys) = (ry, newQueue)
  where
    (ry:rys) = reverse ys
    newQueue = Queue rys []

getFront (Queue (x:xs) ys) = (x, Queue (x:xs) ys)
getFront (Queue [] ys) = (ry, newQueue)
  where
    (ry:rys) = reverse ys
    newQueue = Queue (ry:rys) []

insertRear y (Queue x ys) = Queue x (y:ys)

