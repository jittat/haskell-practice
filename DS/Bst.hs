data BST a = Nil | Node a (BST a) (BST a)
  deriving Show

empty :: (Ord a, Eq a) => BST a -> Bool
empty Nil = True
empty _ = False

insert :: (Ord a, Eq a) => a -> BST a -> BST a
insert x Nil = Node x Nil Nil
insert x (Node y left right) = if x == y
  then Node y left right
  else if x < y
       then Node y (insert x left) right
       else Node y left (insert x right)

contains :: (Ord a, Eq a) => a -> BST a -> Bool
contains x Nil = False
contains x (Node y left right) = if x == y
  then True
  else if x < y
       then contains x left
       else contains x right

minElt :: (Ord a, Eq a) => BST a -> a
minElt (Node x Nil _) = x
minElt (Node x left _) = minElt left

maxElt :: (Ord a, Eq a) => BST a -> a
maxElt (Node x _ Nil) = x
maxElt (Node x _ right) = maxElt right

                              
