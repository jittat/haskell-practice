module Graph.Bfs
  (bfs)
where

import Data.Map (Map)
import qualified Data.Map as Map

import Graph.Graph
import DS.Queue

doBfs :: Integer -> Queue Integer -> Map Integer Bool -> Map Integer Integer -> Graph -> Map Integer Integer
doBfs s queue found layers graph = if isQueueEmpty queue
  then layers
  else let
    (u, restQueue) = extractFront queue
    d = layers Map.! u
    vs = (edges graph) Map.! u
    newVertices = filter (\x -> ((Map.findWithDefault False x found) == False)) vs
    newFound = foldl (\m u -> (Map.insert u True m)) found newVertices 
    newLayers = foldl (\m u -> (Map.insert u (d + 1) m)) layers newVertices
    newQueue = foldl (\q u -> insertRear u q) restQueue newVertices
  in
    doBfs s newQueue newFound newLayers graph

bfs :: Integer -> Graph -> Map Integer Integer
bfs s graph = doBfs s (initQueue [s]) (Map.fromList [(s,True)]) (Map.fromList [(s,0)]) graph

