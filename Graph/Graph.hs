module Graph.Graph
  ( Graph
  , buildGraph
  , readGraph
  , numVertices
  , numEdges
  , edges
  , vertices
  ) where

import Data.Map (Map)
import qualified Data.Map as Map

data Graph = Graph { numVertices :: Integer
                   , numEdges :: Integer
                   , vertices :: [Integer]
                   , edges :: Map Integer [Integer]
                   } deriving (Show)

buildGraph :: Integer -> Integer -> [[Integer]] -> Map Integer [Integer] -> Graph
buildGraph n m [] emap = Graph n m [1 .. n] emap
buildGraph n m (e:es) emap = buildGraph n m es newMap
  where
    (u:v:_) = e
    newList1 = v:(emap Map.! u)
    newMapTmp = Map.insert u newList1 emap
    newList2 = u:(emap Map.! v)
    newMap = Map.insert v newList2 newMapTmp

readGraph = do contents <- getContents
               let (l1:ll) = lines contents
               let [n,m] = fmap (\x -> read x :: Integer) (words l1)
               let edgeList = fmap (\l -> (fmap (\y -> read y :: Integer) (words l))) ll
               let emptyMap = Map.fromList (fmap (\x -> (x, [])) [1 .. n])
               return (buildGraph n m edgeList emptyMap)

