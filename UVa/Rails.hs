rcheck :: [Integer] -> [Integer] -> [Integer] -> Bool
rcheck [] [] [] = True
rcheck [] (o:os) (s:ss) | o /= s   = False
rcheck is (o:os) (s:ss) | o == s    = rcheck is os ss
rcheck (i:is) (o:os) ss | i == o    = rcheck is os ss
                        | i /= o    = rcheck is (o:os) (i:ss)

railsCheck :: [Integer] -> [Integer] -> Bool
railsCheck input output = rcheck input output []

doSolve :: Integer -> [[Integer]] -> [String]
doSolve 0 _ = []
doSolve n [[0]] = [""]
doSolve n ([0]:[nn]:rest) = ("":(doSolve nn rest))
doSolve n (input:rest) = let
  c = railsCheck [1..n] input
  in if c
     then ("Yes":(doSolve n rest))
     else ("No":(doSolve n rest))

solve :: [[Integer]] -> [String]
solve ([n]:rest) = doSolve n rest

lineToIntList :: String -> [Integer]
lineToIntList l = fmap (\y -> read y :: Integer) (words l)

main :: IO ()
main = do contents <- getContents
          let ll = lines contents
          let ilist = fmap (\l -> lineToIntList l) ll
          let results = solve ilist
          putStr (unlines results)
